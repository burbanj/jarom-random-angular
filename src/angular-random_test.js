/* jshint esversion:6 */
import './angular-random';

describe('jarom.random', function () {
    beforeEach(angular.mock.module('jarom.random'));
    
    it('is defined',angular.mock.inject(['tools',function(tools){
        expect(tools).toBeDefined();
    }]));

    it('can get a random paragraph',angular.mock.inject(['tools',function(tools){
        var para=tools.randomParagraph();
        expect(para).toBeDefined();
    }]));
    it('can pick from a provided list of items',angular.mock.inject(['tools',function(tools){
        var seed=['one','two','three'];
        var oneItem=tools.pickRandomItems(seed,1);
        expect(oneItem.length).toBe(1);
        expect(seed.includes(oneItem[0])).toBe(true);
        var twoItems=tools.pickRandomItems(seed,2);
        expect(twoItems.length).toBe(2);
        expect(seed.includes(twoItems[0])).toBe(true);
        expect(seed.includes(twoItems[1])).toBe(true);
        expect(twoItems[0] == twoItems[1]).toBeFalsy();
    }]));
    it('can pick a number up to _max',inject(['tools',function(tools){
        var num=tools.rndUpTo(4);
        expect(num).toBeLessThanOrEqual(4);
        expect(num).toBeGreaterThanOrEqual(1);
    }]));
});