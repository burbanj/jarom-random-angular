angular.module('jarom.random',[])
.factory('tools',function(){
	var books={
		alice:require('./alice.html'),
		moby:require('./2701-h.html')
	};
	var defaultBook=pickRandomItems(Object.keys(books),1)[0];
	/**
	 * @param {Array} _items - An array of any type
	 * @param {number}  _count - Number of items to return
	 * @return {Array} Array of random items from _item 
	 */
	function pickRandomItems(_items,_count) {
		var retval=_items.slice();
		while (retval.length > _count) {
			idx = Math.floor(Math.random() * retval.length);
			retval.splice(idx, 1); //remove random item
		}
		return retval;
	}
	/**
	 * @param {number} _max - upper bound
	 * @returns {number} A number between 1 and _max
	 */
	function rndUpTo(_max) {
		return Math.floor(Math.random() * _max) + 1;
	}
	/**
	 * Picks a paragraph at random from alice in wonderland or moby dick
	 * @param {number} count - number of paragraphs to return
	 * @param {string} book - name of a book to draw from
	 */
	function randomParagraph(count,book) {
		if (count === undefined) { count=1;}
		if (book === undefined) {
			book=defaultBook;
		}
		var paragraphs=books[book].html.body[0].p.filter(function(p){
			return typeof p === 'string';
		});
		var startpoint=Math.round(Math.random()*paragraphs.length);
		var txt=paragraphs.slice(startpoint,startpoint + count);//.join(' ');
		return txt.join(' ');
	}
	return {
		pickRandomItems:pickRandomItems,
		randomParagraph:randomParagraph,
		rndUpTo:rndUpTo
	};
});