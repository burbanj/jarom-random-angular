const path = require('path');
const webpack = require('webpack');  

module.exports = {
  entry: {
    app: './src/angular-random.js'
  },
  output: {
    filename: 'angular-random.js',
    path: path.resolve(__dirname, './dist')
  },
  module: {
    rules: [
      {
        test: /\.js$/, // include .js files
        enforce: "pre", // preload the jshint loader
        exclude: /node_modules/, // exclude any and all files in the node_modules folder
        use: [
          {
            loader: "jshint-loader"
          }
        ]
      },
      { 
        test: /\.html$/, 
        use: [
          {loader:'xml-loader'}
        ]
      }
    ]
  },
  plugins:[
  ]
};
