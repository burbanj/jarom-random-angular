//jshint strict: false
module.exports = function(config) {
  config.set({

    basePath: './',

    files: [
      './node_modules/angular/angular.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './test-context.js',
    ],

    autoWatch: true,

    frameworks: ['jasmine'],

    browsers: ['Chrome'],
    

    preprocessors: {
      'test-context.js':['webpack']
    },

    webpack: {
      mode:'development',
      module: {
        rules: [
          {
            test: /\.js$/, // include .js files
            enforce: "pre", // preload the jshint loader
            exclude: /node_modules/, // exclude any and all files in the node_modules folder
            use: [
              {
                loader: "jshint-loader"
              }
            ]
          },
          { 
            test: /\.html$/, 
            use: ['xml-loader']
          }
        ]
      },
      watch:true
    },
    webpackServer:{
      noInfo:true
    }

  });
};
